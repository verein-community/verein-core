﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemsCollection", menuName = "ModularFPS/Items Collection")]
public class ItemsCollection : ScriptableObject
{

    public Item[] items;

    public int Length { get { return items.Length; } }

    public Item this[int i]
    {
        get
        {
            return items[i];
        }
        set
        {
            items[i] = value;
        }
    }
}
