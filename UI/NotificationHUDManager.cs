﻿using UnityEngine;

public class NotificationHUDManager : MonoBehaviour
{
    [Tooltip("UI panel containing the layoutGroup for displaying notifications")]
    public RectTransform notificationPanel;
    [Tooltip("Prefab for the notifications")]
    public GameObject notificationPrefab;


    void Awake()
    {
#if MODULARFPS
        PlayerWeaponsModule playerWeaponsModule = FindObjectOfType<PlayerCharacterController>().modules.FindModuleOfType<PlayerWeaponsModule>();
        playerWeaponsModule.onAddedWeapon += OnPickupWeapon;
#endif
    }

    void OnUpdateObjective(UnityActionUpdateObjective updateObjective)
    {
        if (!string.IsNullOrEmpty(updateObjective.notificationText))
            CreateNotification(updateObjective.notificationText);
    }

#if MODULARFPS
    void OnPickupWeapon(Item item, WeaponController weaponController, int index)
    {
        if (index != 0)
            CreateNotification("Picked up weapon : " + item.itemName);
    }
#endif

    public void CreateNotification(string text)
    {
        GameObject notificationInstance = Instantiate(notificationPrefab, notificationPanel);
        notificationInstance.transform.SetSiblingIndex(0);

        NotificationToast toast = notificationInstance.GetComponent<NotificationToast>();
        if (toast)
        {
            toast.Initialize(text);
        }
    }

    public void RegisterObjective(Objective objective)
    {
        objective.onUpdateObjective += OnUpdateObjective;
    }

    public void UnregisterObjective(Objective objective)
    {
        objective.onUpdateObjective -= OnUpdateObjective;
    }
}
