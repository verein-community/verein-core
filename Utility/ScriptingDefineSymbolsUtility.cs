﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class ScriptingDefineSymbolsUtility
{
    /// <summary>
    /// Adds scripting define symbol if it's not present in player settings
    /// </summary>
    /// <param name="symbol">This is what we will define</param>
    public static void AddDefineSymbol(string symbol, BuildTargetGroup buildTargetGroup)
    {
        string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

        if (!symbols.Contains(symbol))
        {
            string result = symbols.Length > 0 ? symbols + $";{symbol}" : symbol;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, result);
        }
    }
}
#endif