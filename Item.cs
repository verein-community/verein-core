﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Item : ScriptableObject
{

    [Tooltip("Sound played on use")]
    public AudioClip useSFX;

    [Header("Information")]
    [Tooltip("The ID of this item to look for it in code")]
    public string itemID;
    [Tooltip("The name that will be displayed in the UI for this item")]
    public string itemName;
    [Tooltip("The picture that will be displayed in the UI for this item")]
    public Sprite itemIcon;
    [Tooltip("The weight of the item in inventory")]
    public float itemWeight;
    [Tooltip("Specifies if item can be used from inventory or by hotkey")]
    public bool useable;
    [Tooltip("Specifies if item should be removed from inventory after use")]
    public bool singleUse;

    [HideInInspector]
    public ItemsCollection currentCollection;
    [HideInInspector]
    public int currentIndex;

    public virtual void Use()
    {
        if (singleUse)
            currentCollection.items[currentIndex] = null;
    }

#if MODULARFPS
    public virtual void Use(PlayerCharacterController player)
    {
        if(singleUse)
            currentCollection.items[currentIndex] = null;

        if (useSFX)
            AudioUtility.CreateSFX(useSFX, player.transform.position, AudioUtility.AudioGroups.Pickup, 0f);
    }
#endif


}
