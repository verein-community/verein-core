# Verein Core

![Verein Logo](https://gitlab.com/verein-community/verein-core/-/raw/master/Images/verein_logo.png)

Verein Core is a bundle of assets which serves as a base for other Verein products, as well as it can serve as an alone base for most of the games

## References

[https://github.com/ditzel/SimpleIK](https://github.com/ditzel/SimpleIK "SimpleIK")
